$(function(){

    var d = new Date();
    var month = d.getMonth()+1;
    var november = 11;
    var december = 12;

    // 11月or12月以外ってことやで
    if(month < november)
    {
        $('#year').find(':last').hide();
    }

    $('#year').focusout(function(){
        if($(this).val() == $(this).find(':last').val())
        {
            if(month == november)
            {
                // 今月が11月なら1月のみ表示
                $('#month').children().each(function(index, element){
                    if($(this).val() > 1)
                    {
                        $('#month').val('1');
                        $(this).hide();
                    }
                });
            }
            else if(month == december)
            {
                //今月が12月なら1・2月を表示
                $('#month').children().each(function(){
                    if($(this).val() > 2)
                    {
                        $('#month').val('1');
                        $(this).hide();
                    }
                });
            }
            else
            {
                // 11月か12月の場合以外は来年出てないはずなんですけど……
                console.log('errorなんですけど');
            }
        }
        else
        {
            $('#month').children().each(function(){
                $(this).show();
            });
        }
    });

});