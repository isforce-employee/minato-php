<!DOCTYPE html>
<html>
    <head><?=$head?></head>
    <body>
        <div id="global_navi">
            <?=$global_navi?>
        </div>
        <div id="container">
            <?=$content?>
        </div>
        <div id="footer">
            <?=$footer?>
        </div>
    </body>
</html>