<div id="global_navi_logo">
    <a href="/workreport/"><img src="/assets/img/site_logo.png" alt="グローバルナビロゴ" /></a>
</div>
<div id="global_navi_links">
    <?php foreach($links as $key => $val): ?>
        <?php if($key === $page_type): ?>
            <a class="link_button current"><?=$val['name']?></a>
        <?php else: ?>
            <a href="<?=$val['uri']?>" class="link_button"><?=$val['name']?></a>
        <?php endif; ?>
    <?php endforeach; ?>
</div>