<h1><?=$title?></h1>
<div class="content">
    <p class="text">
    ここには使用者の設定を入力/確認するページを作る予定です。
    保持方法を検討する必要があります。
    </p>

    <form action="" method="post">
        <ul>
            <label for="user_name">お名前</label>
            <li><input type="text" id="user_name" value="" placeholder="アイエス太郎" /></li>
            <label for="user_id">社員番号</label>
            <li><input type="text" id="user_id" value="" placeholder="M00-0000" /></li>
            <li><input type="submit" id="regist_btn" value="登録する"></li>
        </ul>
    </form>
</div>