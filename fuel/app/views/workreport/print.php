<h1><?=$title?></h1>
<div class="content">
    <p class="text">
        ここにはボタン一つで当月分の業務報告書と翌月分の勤務予定表を印刷する機能を作成する予定です。
        もちろん、休暇申請書を同時に印刷してもサイズがおかしくなったりしないようにします。 
        印刷できるのは5年前から２か月後まで、そのうちjsでフォームのアレをソレしたい。
    </p>

    <form action="" method="get">
        <select id="year">
            <?php for($i=$this_y-5; $i <= $this_y+1; $i++): ?>
                <option value="<?=$i?>" id="<?=$i?>" <?php if($i == $this_y) echo 'selected'; ?> ><?=$i?>年</option>
            <?php endfor; ?>
        </select>
        <select id="month">
            <?php for($i=1; $i <= 12; $i++ ): ?>
                <option value="<?=$i?>" <?php if($i == $this_m) echo 'selected'?>><?=$i?>月分</option>
            <?php endfor; ?>
        </select>
        <input type="submit" id="print_btn" value="印刷する" />
    </form>
</div>