<?php
class Presenter_workreport_base extends Presenter
{
    var $common_head        = 'common_head';
    var $common_footer      = 'common_footer';
    var $common_global_navi = 'common_global_navi';

    /**
     * 共通メソッド(error除く)
     */
    public function before()
    {
        // ページごとの設定ファイルを取得
        Config::load('workreport', true);
        $this->workreport_conf = Config::get('workreport.'.$this->page_type);
        // ページタイトルを設定
        $this->title = $this->workreport_conf['title'];

        // 共通でヘッダをセット
        $this->head = View::forge(
            $this->common_head,
            array('title' => $this->title)
        );

        // 共通でグローバルナビをセット
        $global_navi_links = Config::get('workreport.global_navi', false);
        if($this->workreport_conf['use_global_navi'])
        {
            $this->global_navi = View::forge(
                $this->common_global_navi,
                array(
                    'links' => $global_navi_links,
                    'page_type' => $this->page_type,
                )
            );
        }

        // 共通でフッターをセット
        $this->footer = View::forge($this->common_footer);

        // ページ固有のjsを読み込み(ページ名.jsとすること)
        if(Asset::find_file('page_unique/'.$this->page_type.'.js', 'js'))
        {
            Asset::js(array('page_unique/'.$this->page_type.'.js'), array(), 'page_unique_js', false);
        }
    }

    /**
     * contentにviewをforgeするラッパー
     */
    public function content_forge($data, $page_type = false)
    {
        if($page_type)
        {
            $this->content = View::forge($page_type, $data);
        }
        else
        {
            $this->content = View::forge('workreport/'.$this->page_type, $data);
        }
    }
}