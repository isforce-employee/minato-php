<?php
class Presenter_workreport_setting extends Presenter_workreport_base
{
    /**
     * 共通メソッド呼び出し
     */
    public function before()
    {
        parent::before();
    }

    /**
     * 固有メソッド
     */
    public function view()
    {
        $data['title'] = $this->title;

        parent::content_forge($data);
    }
}