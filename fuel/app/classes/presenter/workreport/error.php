<?php
class Presenter_workreport_error extends Presenter_workreport_base
{
    var $common_head        = 'common_head';
    var $common_footer      = 'common_footer';
    var $common_global_navi = 'common_global_navi';

    // 共通メソッドは使わないのでbeforeをオーバーライド
    public function before()
    {
        // ページタイトルを設定
        $this->title = '404 not found';

        // ヘッダをセット
        $this->head = View::forge(
            $this->common_head,
            array('title' => $this->title)
        );

        // グローバルナビをセット
        Config::load('workreport', true);
        $global_navi_links = Config::get('workreport.global_navi', false);
        $this->global_navi = View::forge(
            $this->common_global_navi,
            array(
                'links' => $global_navi_links,
                'page_type' => 'error',
            )
        );

        // フッターをセット
        $this->footer = View::forge($this->common_footer);

    }

    /**
     * 固有メソッド
     */
    public function view()
    {
        $data['title'] = 'not found';

        parent::content_forge($data, 'error/not_found');
    }
}