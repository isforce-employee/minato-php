<?php
class Presenter_workreport_webaps extends Presenter_workreport_base
{
    /**
     * 共通メソッド呼び出し
     */
    public function before()
    {
        parent::before();
    }

    /**
     * 固有メソッド
     */
    public function view()
    {
        // @TODO unとpsは暫定なので実装しないように。検証するときは入力してね。
        $un = '';
        $ps = '';
        $data['title'] = $this->title;

        //$url = 'https://www.uet.jp/';
        $url = 'https://members.uet.jp/members_frame.htm';
        //$url = 'https://webapps.uet.jp/WorkReport/WorkSchedule201003.php';

        // @TODO ベーシック認証が突破できないのを解決する必要がある。
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // 自己証明書なので無視
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // ベーシック認証にユーザー名とパスワードを渡す
        curl_setopt($curl, CURLOPT_USERPWD, $un.":".$ps);

        $data['html'] = mb_convert_encoding(curl_exec($curl), 'utf-8', 'ISO-2022-JP');

        // デバッグ用エラーコード出力
        //$res_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        parent::content_forge($data);

    }
}