<?php
/**
 * ハロー業務報告書
 */
class Controller_Workreport extends Controller
{
    // 共通テンプレートをセット
    var $template       = 'common_template';
    var $page_type      = 'index';

    public function before()
    {
        // fuelのお約束
        parent::before();

        if($this->param('page_type'))
        {
            $this->page_type = $this->param('page_type');
        }
    }

    /**
     * The 404 action for the application.
     *
     * @access  public
     * @return  Response
     */
    public function action_404()
    {
        return Presenter::forge('workreport/error', 'view', false, $this->template);
    }

    /**
     * トップ画面
     */
    public function action_index()
    {
        return self::view_forge();
    }

    /**
     * 印刷ページ
     */
    public function action_print()
    {
        return self::view_forge();
    }

    /**
     * 設定ページ
     */
    public function action_setting()
    {
        return self::view_forge();
    }

    /**
     * 業務報告書を表示するページ
     */
    public function action_webaps()
    {
        return self::view_forge();
    }

    /**
     * View::forgeラッパー
     */
    public function view_forge()
    {
        return Presenter::forge('workreport/'.$this->page_type, 'view', false, $this->template)->set('page_type', $this->page_type);
    }

} 