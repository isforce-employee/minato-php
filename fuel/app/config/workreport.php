<?php
return array(
    'index' => array(
        'title' => 'hello,workreport',
        'use_global_navi' => true,
    ),
    'print' => array(
        'title' => 'hello,workreport(print)',
        'use_global_navi' => true,
    ),
    'setting' => array(
        'title' => 'hello,workreport(setting)',
        'use_global_navi' => true,
    ),
    'webaps' => array(
        'title' => 'hello,workreport(webaps)',
        'use_global_navi' => true,
    ),
    'global_navi' => array(
        'index' => array(
            'uri'  => '/workreport/',
            'name' => 'home',
        ),
        'print' => array(
            'uri'  => '/workreport/print/',
            'name' => 'print',
        ),
        'setting' => array(
            'uri'  => '/workreport/setting/',
            'name' => 'setting',
        ),
        'webaps' => array(
            'uri'  => '/workreport/webaps/',
            'name' => 'webaps',
        ),
        'dummy1' => array(
            'uri'  => '/workreport/',
            'name' => 'dummy1',
        ),
        'dummy2' => array(
            'uri'  => '/workreport/',
            'name' => 'dummy2',
        ),
    ),
);