<?php
return array(
	'_root_'  => 'workreport/index',  // The default route
	'_404_'   => 'workreport/404',    // The main 404 route
	
	'workreport/' => 'workreport/index',
	'workreport/(?P<page_type>print|setting|webaps)' => 'workreport/$1',
);
